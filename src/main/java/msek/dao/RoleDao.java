package msek.dao;

import msek.model.AppRole;

public abstract class RoleDao extends BasicDao {
    public abstract AppRole findRoleByName(String name);
    public abstract void save(AppRole appRole);
}
